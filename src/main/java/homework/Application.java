package homework;

import java.util.Scanner;

/**
 * The class that contains all homework tasks
 * @author  Voronovskyi Danylo
 * @version 1
 */

public class Application {

    public static void main(String[] args) {

        /** Creating new scanner to read from console */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the interval, enter first value: ");
        /** @Param n - first value for interval */
        int n = scanner.nextInt();

        System.out.println("enter last value: ");
        /** @Param n - last value for interval */
        int m = scanner.nextInt();

        /** @Param Array[] - new array */
        int Array [] = new int[m];
        /** @Param oddSum - sum of odd numbers */
        int oddSum = 0;
        /** @Param evenSum - sum of even numbers */
        int evenSum = 0;

        for (int i = n; i < m; i++) {
            Array[i] = i;
        }

        System.out.println("Print odd numbers from start to the end :");

        for (int i = n; i < m; i++) {
            if (Array[i] % 2 == 1) {
                oddSum += i;
                System.out.println(Array[i]);
            }
        }

        System.out.println("Sum of odd : " + oddSum);

        System.out.println("Print even number from the end to start :");

        for (int i = m - 1; i >= n; i--) {
            if (Array[i] % 2 == 0) {
                evenSum += i;
                System.out.println(Array[i]);
            }
        }

        System.out.println("Sum of even : " + evenSum);

        System.out.println("Enter size of FibonacciSet : ");

        int size = scanner.nextInt();
        /** @Param Array[] - new array for Fibonacci task */
        int [] fibonacciArray = new int [size];
        /** @Param firstNumber - first number of Fibonacci array */
        int firstNumber = 1;
        /** @Param secondNumber - second number of Fibonacci array */
        int secondNumber = 1;
        int fibonacciNumber = 0;

        System.out.println("Fibonacci : ");
        System.out.println(firstNumber);
        System.out.println(secondNumber);

        for (int i = 0; i < size; i++) {
            fibonacciNumber = firstNumber + secondNumber;
            fibonacciArray[i] = fibonacciNumber;
            System.out.println(fibonacciArray[i]);
            firstNumber = secondNumber;
            secondNumber = fibonacciNumber;
        }

        double oddPercantage = 0;
        double evenPercantage = 0;
        int maxEven = 0;
        int maxOdd = 0;
        int evenCal = 0;

        for (int i = 0; i < size; i++) {
            if (fibonacciArray[i] % 2 == 0) {
                evenCal++;
                if (maxEven < fibonacciArray[i]) {
                    maxEven = fibonacciArray[i];
                }
            } else {
                if(maxOdd < fibonacciArray[i]){
                    maxOdd = fibonacciArray[i];
                }
            }
        }

         final int percentage = 100;

        /** @Param evenPercentage - count percentage of even numbers */
        evenPercantage = (evenCal * percentage) / size;
        /** @Param oddPercentage - count percentage of odd numbers*/
        oddPercantage = percentage - evenPercantage;
        System.out.println("Even percentage : " + evenPercantage);
        System.out.println("Odd percentage : " + oddPercantage);
        System.out.println("Max even : " + maxEven);
        System.out.println("Max odd : " + maxOdd);



    }

}
